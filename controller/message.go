package controller

import (
	"chat-app/models"
	"fmt"
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	// "strings"

	// "fmt"
	"net/http"
	// "strconv"
	"time"

	// jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

var Flag string

type TempMessage struct {
	Content        string `json:"content" binding:"required"`
	SendToId       int    `json:"sendto"`
	ReceivedFromId int    `json:"receivedfrom"`
}

func SendMessage(c *gin.Context) {

	var tempMessage TempMessage
	var Role models.UserRole
	// var count int64
	// var
	// var User models.User

	// id, _ := models.Rdb.HGet("user", "ID").Result()
	// ID, _ := strconv.Atoi(id)

	roleId, _ := models.Rdb.HGet("user", "RoleID").Result()

	if roleId == "" {
		fmt.Println("Redis empty....checking Database for user...")
		err := FillRedis(c)
		if err != nil {
			c.JSON(404, gin.H{
				"error": "something went wrong with redis",
			})
			return
		}
	}
	roleId, _ = models.Rdb.HGet("user", "RoleID").Result()

	if roleId != "1" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Message can only send by admin"})
		return
	}

	if err := c.ShouldBindJSON(&tempMessage); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Message": err.Error()})
		return
	}

	err := models.DB.Where("role= ?", "admin").First(&Role).Error
	if err != nil {
		fmt.Println("err ", err.Error())
		return
	}

	NewMessage := models.Message{
		Content:        tempMessage.Content,
		CreatedAt:      time.Now(),
		SendToId:       tempMessage.SendToId,
		ReceivedFromId: tempMessage.ReceivedFromId,
	}

	// Flag := strings.Index(Content, "@")

	// if Flag == "text" {

	// 	models.DB.Where("text = ?", Content).Find(&Message).Count(&count)
	// 	if count == 0 {
	// 		return nil, jwt.ErrFailedAuthentication
	// 	}
	// } else if Flag == "image" {
	// 	models.DB.Where("text = ?", Content).Find(&MessageImage).Count(&count)
	// 	if count == 0 {
	// 		return nil, jwt.ErrFailedAuthentication
	// 	}
	// }

	errs := models.DB.Create(&NewMessage).Error
	NewMessageCache(c, NewMessage)
	if errs != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, NewMessage)
	return
}

func GetAllMessages(c *gin.Context) {
	var messages []models.Message

	models.DB.Find(&messages)

	c.JSON(http.StatusOK, messages)
	fmt.Println(models.Rdb.LRange("message", 0, -1).Result())

}

// func GetMessage(c *gin.Context) {
// var xistingMessages models.Message
// Country := c.Query("")
// name := c.Query("name")
// c.JSON(http.StatusOK, "Hello %s %s", country, name)
// }

func GetMessage(c *gin.Context) {

	country := c.Param("country")
	name := c.Query("name")

	// message := c.Query("")
	var message string
	//  c.JSON()
	// GET FROM CACHE FIRST

	switch {

	case country == "India":

		message = "Namskar " + name

	case country == "US":

		message = "Hello " + name
		
	case country == "Germany":

		message = "Bonjour " + name

	}
	c.JSON(http.StatusOK, gin.H{"country": country, "name": name, "message": message})
}

// // Check if the product already exists.
// err := models.DB.Where("id = ?", c.Param("id")).First(&existingMessages).Error
// if err != nil {
// 	c.JSON(http.StatusBadRequest, gin.H{"error": "msg doesnot exists."})
// 	return
// }

// GET FROM CACHE FIRST
// c.JSON(http.StatusOK, gin.H{"msg": existingMessages})

func DeleteMessage(c *gin.Context) {

	// Get model if exist
	var message models.Message

	if err := models.DB.Where("id = ?", c.Param("id")).First(&message).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&message)

	c.JSON(http.StatusOK, gin.H{"msg": "Deleted successfully"})

}
type UploadedFile struct {
	Status    bool
	MessageId int
	Filename  string
	Path      string
	Err       string
}

func generateFilePath(id string, extension string) string {
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	newFileName := uuid.New().String() + extension

	fmt.Println(newFileName)
	projectFolder, err := os.Getwd()

	if err != nil {
		log.Fatal(err)
	}

	imageFolder := filepath.ToSlash(projectFolder) + "/image/"
	messageImageFolder := imageFolder + id + "/"

	fmt.Println(messageImageFolder)

	if _, err := os.Stat(messageImageFolder); err != nil {
		os.MkdirAll(messageImageFolder, os.ModeDir)
		fmt.Println("Uploaded Image to folder")
	}

	imagePath := messageImageFolder + newFileName
	return imagePath
}

func SaveToBucket(c *gin.Context, f *multipart.FileHeader, extension string, filename string) UploadedFile {
	/*
		whitelist doctionary for extensions
		golang doesnot support "for i in x" construct like python,
		Iterating the list would be expensive, thus we need to use a struct to prevent for loop.
	*/
	acceptedExtensions := map[string]bool{
		".png":  true,
		".jpg":  true,
		".JPEG": true,
		".PNG":  true,
		".JPG":  true,
	}
	id, _ := strconv.Atoi(c.Param("id"))

	if !acceptedExtensions[extension] {
		return UploadedFile{MessageId: id, Filename: filename, Err: "Invalid Extension"}
	}

	filePath := generateFilePath(c.Param("id"), extension)
	fmt.Println(filePath)
	err := c.SaveUploadedFile(f, filePath)

	if err == nil {
		return UploadedFile{
			Status:    true,
			MessageId: id,
			Filename:  filename,
			Path:      filePath,
			Err:       "",
		}
	}
	return UploadedFile{Status: false, MessageId: id, Filename: filename, Err: ""}
}

func UploadMessageImages(c *gin.Context) {

	form, _ := c.MultipartForm()
	files := form.File["file"]

	var SuccessfullyUploadedFiles []UploadedFile
	var UnSuccessfullyUploadedFiles []UploadedFile
	var MessageImages []models.MessageImage

	for _, f := range files {
		//save the file to specific dst
		extension := filepath.Ext(f.Filename)
		fmt.Println(extension)
		uploaded_file := SaveToBucket(c, f, extension, f.Filename)
		if uploaded_file.Status {
			SuccessfullyUploadedFiles = append(SuccessfullyUploadedFiles, uploaded_file)
			MessageImages = append(MessageImages, models.MessageImage{
				ImagePath: uploaded_file.Path,
				MessageId: uploaded_file.MessageId,
			})

		} else {
			UnSuccessfullyUploadedFiles = append(UnSuccessfullyUploadedFiles, uploaded_file)
		}
	}

	models.DB.Create(&MessageImages)

	c.JSON(http.StatusOK, gin.H{
		"successful": SuccessfullyUploadedFiles, "unsuccessful": UnSuccessfullyUploadedFiles,
	})

}

// if content=text
// else content=file
