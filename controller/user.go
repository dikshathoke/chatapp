package controller

import (
	"chat-app/models"
	"fmt"
	"net/http"
	"strconv"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

type TempUser struct {
	Phone           string `json:"phone" binding:"required"`
	NickName        string `json:"nickname" binding:"required"`
	Password        string `json:"password" binding:"required"`
	ConfirmPassword string `json:"confirm_password" binding:"required"`
}

// Returns a custom user input
func ReturnParameterMissingError(c *gin.Context, parameter string) {
	var err = fmt.Sprintf("Required parameter %s missing.", parameter)
	c.JSON(http.StatusBadRequest, gin.H{"error": err})
}

type login struct {
	Phone    string `form:"phone" json:"phone" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

func Signin(c *gin.Context) (interface{}, error) {
	/* THIS FUNCTION IS USED BY THE AUTH MIDDLEWARE. */
	var loginVals login
	var user models.User
	var count int64

	if err := c.ShouldBind(&loginVals); err != nil {
		return "", jwt.ErrMissingLoginValues
	}
	phone := loginVals.Phone
	// First check if the user exist or not...
	models.DB.Where("phone = ?", phone).Find(&user).Count(&count)
	if count == 0 {
		return nil, jwt.ErrFailedAuthentication
	}

	if len(phone) != 10 {
		c.JSON(400, gin.H{
			"error": "Mobile Number is wrong",
		})
	}

	if CheckCredentials(loginVals.Phone, loginVals.Password, models.DB) {
		NewRedisCache(c, user)
		return &models.User{
			Phone: phone,
		}, nil

	}
	return nil, jwt.ErrFailedAuthentication
}

// CreateAdmin godoc
// @Description API Endpoint to register the user with the role of Vendor or Admin.
// @Router /api/v1/auth/admin/create [post]
// @Tags admin
// @Accept json
// @Produce json
// @Param login formData TempUser true "Info of the user"
func CreateAdmin(c *gin.Context) {

	// Create a user with the role of admin.
	var tempUser TempUser
	var Role models.UserRole

	if err := c.ShouldBindJSON(&tempUser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Message": err.Error()})
		return
	}

	// Check if both passwords are same.
	if tempUser.Password != tempUser.ConfirmPassword {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Both passwords do not match."})
	}

	// check if the password is strong and matches the password policy
	// length > 8, atleast 1 upper case, atleast 1 lower case, atleast 1 symbol
	ispasswordstrong, _ := IsPasswordStrong(tempUser.Password)
	if !ispasswordstrong {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Password is not strong."})
		return
	}

	// Check if the user already exists.
	if DoesUserExist(tempUser.Phone) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User already exists."})
		return
	}

	encryptedPassword, error := HashPassword(tempUser.Password)
	if error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Some error occoured."})
		return
	}

	err := models.DB.Where("role= ?", "admin").First(&Role).Error
	if err != nil {
		fmt.Println("err ", err.Error())
		return
	}

	if len(tempUser.Phone) != 10 {
		c.JSON(400, gin.H{
			"error": "Mobile Number must be 10 digit",
		})
		return
	}

	SanitizedUser := models.User{
		NickName:   tempUser.NickName,
		Phone:      tempUser.Phone,
		Password:   encryptedPassword,
		UserRoleID: Role.ID,
		CreatedAt:  time.Now(),
		CreatedBy:  Role.ID,
	}

	errs := models.DB.Create(&SanitizedUser).Error
	if errs != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Some error occoured databse"})
		return
	}
	c.JSON(http.StatusCreated, gin.H{"msg": "User created successfully"})
	return
}

// add member godoc
// @Summary AddMember endpoint is used by the admin role user to add a new member.
// @Description AddMember endpoint is used by the admin role user to add a new member
// @Router /api/v1/auth/member/add [post]
// @Tags member
// @Accept json
// @Produce json
// @Param name formData string true "name of the member"
func AddMember(c *gin.Context) {

	var tempUser TempUser
	var Role models.UserRole

	id, _ := models.Rdb.HGet("user", "ID").Result()
	ID, _ := strconv.Atoi(id)
	roleId, _ := models.Rdb.HGet("user", "RoleID").Result()

	if roleId == "" {
		fmt.Println("Redis empty....checking Database for user...")
		err := FillRedis(c)
		if err != nil {
			c.JSON(404, gin.H{
				"error": "something went wrong with redis",
			})
			return
		}
	}

	roleId, _ = models.Rdb.HGet("user", "RoleID").Result()

	if roleId != "1" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Members can only be added by user"})
		return
	}

	// claims := jwt.ExtractClaims(c)
	// user_phone := claims["phone"]
	// var User models.User

	// // Check if the current user had admin role.
	// if err := models.DB.Where("phone=?", user_phone).First(&User).Error; err != nil {
	// 	c.JSON(http.StatusBadRequest, gin.H{"error": "user does not exist"})
	// 	return
	// }

	// if User.UserRoleID != 1 {
	// 	c.JSON(http.StatusBadRequest, gin.H{"error": "not admin"})
	// 	return
	// }
	// Create a user with the role of member.
	if err := c.ShouldBindJSON(&tempUser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Message": err.Error()})
		return
	}

	// Check if both passwords are same.
	if tempUser.Password != tempUser.ConfirmPassword {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Both passwords do not match."})
	}

	// check if the password is strong and matches the password policy
	// length > 8, atleast 1 upper case, atleast 1 lower case, atleast 1 symbol
	ispasswordstrong, _ := IsPasswordStrong(tempUser.Password)
	if ispasswordstrong == false {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Password is not strong."})
		return
	}

	// Check if the user already exists.
	if DoesUserExist(tempUser.Phone) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User already exists."})
		return
	}

	encryptedPassword, error := HashPassword(tempUser.Password)
	if error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error occoured in password"})
		return
	}

	err := models.DB.Where("role= ?", "member").First(&Role).Error
	if err != nil {
		fmt.Println("err ", err.Error())
		return
	}

	if len(tempUser.Phone) != 10 {
		c.JSON(400, gin.H{
			"error": "Mobile Number is wrong",
		})
		return
	}

	SanitizedUser := models.User{
		NickName: tempUser.NickName,
		Phone:    tempUser.Phone,
		Password:   encryptedPassword,
		UserRoleID: Role.ID,
		CreatedAt:  time.Now(),
		CreatedBy:  ID,
	}

	errs := models.DB.Create(&SanitizedUser).Error
	// NewUserCache(c, SanitizedUser)
	if errs != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Some error occoured to create db"})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"msg": "Member added successfully"})
	return
}

// func GetAllUsers(c *gin.Context) {
// 	// var User []models.User
// 	var existingUsers []models.User

// 	roleId, _ := models.Rdb.HGet("user", "RoleID").Result()
// 	if roleId == "" {
// 		fmt.Println("Redis empty")
// 		err := FillRedis(c)
// 		if err != nil {
// 			c.JSON(404, gin.H{
// 				"error": "something went wrong with redis",
// 			})
// 			return
// 		}
// 	}
// 	roleId, _ = models.Rdb.HGet("user", "RoleID").Result()

// 	if roleId != "1" {
// 		c.JSON(404, gin.H{
// 			"error": "unauthorized",
// 		})
// 		return
// 	}
// 	result := models.DB.Find(&existingUsers)
// 	fmt.Println(result)
// 	c.JSON(200, existingUsers)
// }
  
func GetAllUsers(c *gin.Context) {
	var e []models.User

	// id, _ := models.Rdb.HGet("user", "ID").Result()
	// ID, _ := strconv.Atoi(id)

	p := c.Query("page")
	page, _ := strconv.Atoi(p)
	order := c.Query("order")
	err := models.DB.Order(order).Limit(2).Offset((page - 1) * 2).Find(&e).Error
	if err != nil {
		c.JSON(404, gin.H{"message": "no data found"})
		return
	}
	
	c.JSON(200, e)
	// fmt.Println(models.Rdb.LRange("user",0,-1).Result())
}

