package controller

import (
	"chat-app/models"
	"errors"
	"fmt"
	"log"
	"unicode"

	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

func IsPasswordStrong(password string) (bool, error) {
	var IsLength, IsUpper, IsLower, IsNumber, IsSpecial bool

	if len(password) < 8 {
		return false, errors.New("password Length should be more than 6")
	}
	IsLength = true

	for _, v := range password {
		switch {
		case unicode.IsNumber(v):
			IsNumber = true

		case unicode.IsUpper(v):
			IsUpper = true

		case unicode.IsLower(v):
			IsLower = true

		case unicode.IsPunct(v) || unicode.IsSymbol(v):
			IsSpecial = true

		}
	}

	if IsLength && IsLower && IsUpper && IsNumber && IsSpecial {
		return true, nil
	}

	return false, errors.New("password validation failed")

}

func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 8)
	if err != nil {
		log.Fatal("Error in Hashing")
		return "", err
	}
	return string(hashedPassword), err
}

// DoesUserExist is a helper function which checks if the user already exists in the user table or not.
func DoesUserExist(phone string) bool {
	var users []models.User
	err := models.DB.Where("phone=?", phone).First(&users).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false
		}
	}
	return true
}

func NewRedisCache(c *gin.Context, user models.User) {
	//fmt.Println("setCache hit")
	c.Set("user_phone", user.Phone)
	fmt.Println(c.GetString("user_phone"))
	models.Rdb.HSet("user", "username", user.Phone)

	models.Rdb.HSet("user", "ID", user.ID)
	models.Rdb.HSet("user", "RoleID", user.UserRoleID)
	fmt.Println(models.Rdb.HGetAll("user").Result())
}

func NewMessageCache(c *gin.Context, message models.Message) {
	
	models.Rdb.RPush("message",  message.ID)
	models.Rdb.RPush("message",  message.Content)
	models.Rdb.RPush("message",  message.SendTo)
	models.Rdb.RPush("message", message.ReceivedFrom)
	fmt.Println(models.Rdb.LRange("message",0,-1).Result())
}

func FillRedis(c *gin.Context) error {
	var User models.User
	claims := jwt.ExtractClaims(c)
	phone := claims["phone"]

	err := models.DB.Where("phone = ? ", phone).First(&User).Error
	if err != nil {
		return err
	}
	NewRedisCache(c, User)
	return nil
}

func CheckCredentials(userphone, userpassword string, db *gorm.DB) bool {
	// db := c.MustGet("db").(*gorm.DB)
	// var db *gorm.DB
	var User models.User
	// Store user supplied password in mem map
	var expectedpassword string
	// check if the phone exists
	err := db.Where("phone = ?", userphone).First(&User).Error
	if err == nil {
		// User Exists...Now compare his password with our password
		expectedpassword = User.Password
		if err = bcrypt.CompareHashAndPassword([]byte(expectedpassword), []byte(userpassword)); err != nil {
			// If the two passwords don't match, return a 401 status
			log.Println("User is Not Authorized")
			return false
		}
		// User is AUthenticates, Now set the JWT Token
		fmt.Println("User Verified")
		return true
	} else {
		// returns an empty array, so simply pass as not found, 403 unauth
		log.Fatal("ERR ", err)
	}
	return false
}


// func GeneratePagination(c *gin.Context) models.Pagination {
// 	// Initializing default
// 	//	var mode string
// 	limit := 2
// 	page := 1
// 	sort := "id"
// 	query := c.Request.URL.Query()
// 	for key, value := range query {
// 		queryValue := value[len(value)-1]
// 		switch key {
// 		case "limit":
// 			limit, _ = strconv.Atoi(queryValue)
			
// 		case "page":
// 			page, _ = strconv.Atoi(queryValue)
			
// 		case "sort":
// 			sort = queryValue
// 		}
// 	}
// 	return models.Pagination{
// 		Limit: limit,
// 		Page:  page,
// 		Sort:  sort,
// 	}

// }


//  filter function 
// type filter func(string) bool
// {
// 
// }