package routes

import (
	 "chat-app/controller"

	middlewares "chat-app/middleware"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"

	// ginSwagger "github.com/swaggo/gin-swagger"
	// "github.com/swaggo/gin-swagger/swaggerFiles"
)

func PublicEndpoints(r *gin.RouterGroup, authMiddleware *jwt.GinJWTMiddleware) {
	// Generate public endpoints - [ signup] - api/v1/signup

	// r.POST("/signup", controller.Signup)
	// r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.POST("/login", authMiddleware.LoginHandler)
	r.POST("admin", controller.CreateAdmin)
	r.POST("member/add", controller.AddMember)
	r.GET("users", controller.GetAllUsers)

	// message PublicEndpoints
	r.POST("message", controller.SendMessage)
	r.POST("message/:id/image/upload", controller.UploadMessageImages)
	r.GET("messages", controller.GetAllMessages)
	r.GET("message/:id", controller.GetMessage)
	// r.DELETE("message/:id", controller.DeleteMessage)
	r.GET("test/:country", controller.GetMessage)
	// 
}

func GetRouter(router chan *gin.Engine) {
	// gin.ForceConsoleColor()
	r := gin.Default()

	authMiddleware, _ := middlewares.GetAuthMiddleware()

	// Create a BASE_URL - /api/v1
	v1 := r.Group("/api")
	PublicEndpoints(v1, authMiddleware)
	// AuthenticatedEndpoints(v1.Group("auth"), authMiddleware)
	router <- r
}



