package models

import (
	"time"
)

type UserRole struct {
	ID   int    `json:"id"`
	Role string `json:"role"`
}

type User struct {
	ID         int       `json:"id"`
	Phone      string    `json:"phone"`
	NickName   string    `json:"nickname"`
	Password   string    `json:"password"`
	UserRoleID int       `json:"user_role_id"`
	CreatedAt  time.Time `json:"createdat"`
	UpdatedAt  time.Time `json:"updatedat"`
	CreatedBy  int       `json:"createdby"`
}
