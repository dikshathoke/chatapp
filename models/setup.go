package models

import (
	"fmt"
	"os"
	"strconv"

	"github.com/go-redis/redis"
	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB
var Rdb *redis.Client

type Settings struct {
	DB_HOST     string
	DB_NAME     string
	DB_USER     string
	DB_PASSWORD string
	DB_PORT     string
	SMTP_HOST   string
	SMTP_PASS   string
}

func InitializeSettings() Settings {
	DB_HOST := os.Getenv("DB_HOST")
	DB_NAME := os.Getenv("DB_NAME")
	DB_USER := os.Getenv("DB_USER")
	DB_PASSWORD := os.Getenv("DB_PASSWORD")
	DB_PORT := os.Getenv("DB_PORT")

	var addr = os.Getenv("addr")
	var pass = os.Getenv("pass")
	db, _ := strconv.Atoi(os.Getenv("db"))
	fmt.Println(addr, pass, db)
	Rdb = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pass, // no password set
		DB:       db,   // use default DB
	})

	switch {
	case DB_HOST == "":
		fmt.Println("1 Environment variable DB_HOST not set.")
		os.Exit(1)
	case DB_NAME == "":
		fmt.Println("Environment variable DB_NAME not set.")
		os.Exit(1)
	case DB_USER == "":
		fmt.Println("Environment variable DB_USER not set.")
		os.Exit(1)
	case DB_PASSWORD == "":
		fmt.Println("Environment variable DB_PASSWORD not set.") 
		os.Exit(1)
	}

	settings := Settings{
		DB_HOST:     DB_HOST,
		DB_NAME:     DB_NAME,
		DB_USER:     DB_USER,
		DB_PASSWORD: DB_PASSWORD,
		DB_PORT:     DB_PORT,
	}

	return settings
}

func createInitialData() {

	var UserRole = []UserRole{
		{
			ID:   1,
			Role: "admin",
		},
		{
			ID:   2,
			Role: "member",
		},
	}

	err := DB.CreateInBatches(UserRole, 2).Error
	if err != nil {
		fmt.Println("admin roles created successfully")
		fmt.Println(err.Error())
	}
	encryptedPassword, _ := bcrypt.GenerateFromPassword([]byte("SuperPassword@123"), 8)

	// Create an initial admin user.
	err = DB.Create(&User{
		ID:         1,
		NickName:   "admin",
		Phone:      "8010028985",
		UserRoleID: 1,
		Password:   string(encryptedPassword),
	}).Error

	if err != nil {
		fmt.Println("Admin User already exists.")

	}
}

func ConnectDataBase() {

	settings := InitializeSettings()
	url := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable", settings.DB_USER, settings.DB_PASSWORD, settings.DB_HOST, settings.DB_PORT, settings.DB_NAME)

	connection, err := gorm.Open(postgres.Open(url), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}
	fmt.Println("Migrating tables")

	//normal migration
	err = connection.AutoMigrate(&Message{}, &UserRole{}, &MessageImage{}, &User{})
	if err != nil {
		fmt.Println("error in migration: ", err)
	}
	fmt.Println("Done migrating")

	DB = connection
	// fmt.Println("Done migrating")

	createInitialData()
}
