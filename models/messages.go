package models

import "time"

type Message struct {
	ID         int       `json:"id"`
	Content    string    `json:"content"`
	CreatedAt  time.Time `json:"createdat"`
	SendToId int
	SendTo User `gorm:"foreignKey:SendToId" json:"-"`
	ReceivedFromId int
	ReceivedFrom User `gorm:"foreignKey:ReceivedFromId" json:"-"`
}

type MessageImage struct {
	ID        int    `json:"id"`
	ImagePath string `json:"image_path"`
	MessageId int
	Message   Message `gorm:"foreignKey:MessageId"`
}


// SendTo     User       `gorm:"foreignKey:User"`
	// User       User `gorm:"foreignKey:ReceivedFrom" json:"-"`
	// ReceivedFrom User

// 	// SendBy    int
// 	// User      User   `gorm:"foreignKey:SendBy" json:"-"`
// 	// SendTo    string `gorm:"foreignKey:SendTo" json:"-"`
// type SendMessage st ruct {
// 	ID      int    `json:"id"`
// 	Content string `json:"content"`
// 	// SendTo  int
// 	// User    User `gorm:"foreignKey:SendTo" json:"-"`
// 	// meta
// 	CreatedAt time.Time `json:"createdat"`
// }

//  type ReceiveMessage struct {
// 	ID          int `json:"id"`
// 	Content     string
// 	Message Message `gorm:"foreignKey:Content" json:"-"`
// 	ReceivedBy  int
// 	User        User `gorm:"foreignKey:SendTo" json:"-"`
// }

// type Message struct {
// sendby foreignkey
// send to foreignkey
// content
// content type text image

// meta data
// created at
// msg id
// flag is deleted
// }

