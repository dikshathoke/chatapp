FROM golang:alpine AS debug
RUN apk add bash ca-certificates git gcc g++ libc-dev musl-dev
ENV GO111MODULE=on
ENV GOPROXY=https://proxy.golang.org
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN go mod download

# Build the application
RUN go build -o main .

# RUN the delve debugger ####
COPY ./dlv.sh /
RUN chmod +x /dlv.sh
ENTRYPOINT [ "/dlv.sh" ]

# Command to run when starting the container
CMD ["/app/main"]
